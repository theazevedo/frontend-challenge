# Desafio Elevential



O​ ​ que​ ​ você​ ​ achou​ ​ do​ ​ desafio?
Quais​ ​ foram​ ​ as​ ​ maiores​ ​ dificuldades​ ​ encontradas?
O​ ​ que​ ​ poderia​ ​ ser​ ​ melhorado​ ​ no​ ​ resultado?
Quais​ ​ funcionalidades​ ​ você​ ​ julgou​ ​ mais​ ​ importantes​ ​ e ​ ​ por​ ​ quê?
Que​ ​ decisões​ ​ você​ ​ tomou​ ​ e ​ ​ por​ ​ quê?


O que achei do desafio :

Achei um ótimo desafio, bom para por em teste meus conhecimentos, vejo ainda que sou um pouco
fraco no assunto, mas estou melhorando dia apos dia.

Minhas maiores dificuldades:

Aplicar a parte do estilo da page, estou pegando as manhas do desenvolvimento front, logo com o tempo
eu estarei melhor preparado para os próximos desafios.

Melhorias no resultado:

Aparencia em si, as funções, tiveram algumas que não consegui implementar, tenho certeza que dava pra deixar mais bonito
de que como está agora.

Funcionalidade importantes :

Achei a verificação da senha importante, qualquer erro poderia causar um certo transtorno ao usuario.
A verificação de todos os campos são essenciais para um cadastro.

Decisões tomadas:

Resolvi começar pela parte do JS, só criei uns forms para ter algo usavel e comecei a desenvolver a parte de verificação dos campos,
depois disso eu comecei a tratar um pouco a cara do projeto, adicionando twetter bootstrap para me auxiliar em algumas tarefas.

considerações finais:

Agradeço pela oportunidade, sou bem novato no ramo de Dev e é graças a esses tipos de desafios que vemos a quantidade de conteudo
precisamos aprender!